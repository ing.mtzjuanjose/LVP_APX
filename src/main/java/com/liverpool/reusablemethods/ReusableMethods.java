package com.liverpool.reusablemethods;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.SimpleTimeZone;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.idealized.Javascript;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReusableMethods {
	

	WebDriver driver;

	public ReusableMethods(WebDriver driver) {
		
		this.driver = driver;
	}


	public void explicitWait(WebElement element, long waitTimeInSeconds) {
		
	WebDriverWait explicitWait = new WebDriverWait(driver, Duration.ofSeconds(waitTimeInSeconds));
	WebElement elements = null;
	elements= explicitWait.until(ExpectedConditions.visibilityOf(element));
		
		
	}
	
	
	public void implicitWait(long waitTimeInSeconds) {
				
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(waitTimeInSeconds));
		
	}
	
	
	public void sendKeysToElement(WebElement element, String text) {
		
	
		element.click();
		element.clear();
		element.sendKeys(text);
		
		
	}
	
	
	public void scrollToElement(WebElement element, long waitTimeInSeconds) { 
		
//		WebDriverWait explicitWait = new WebDriverWait(driver, Duration.ofSeconds(waitTimeInSeconds));
//		WebElement elements = null;
//		elements= explicitWait.until(ExpectedConditions.visibilityOf(element));
		explicitWait(element, 6);
			
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.perform();
			
	}
	
	public void colorSelection(String colorSelection) {
		
		List<WebElement> colorList = driver.findElements(By.cssSelector("a[id*='variants-normalizedColor-']"));
		System.out.println(colorList.size());
		
		for(WebElement option : colorList) {
			
			if(option.getAttribute("id").equalsIgnoreCase("variants-normalizedColor-" +colorSelection)) {
				System.out.println(option.getAttribute("id"));
				option.click();
				
				break;
				
			}			
		}

	}
	
	public String getDate() {
		 SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
		    Date date = new Date();  
		
		return dateFormat.format(date);
	}
	
	
	
	public void takeScreenShot() throws IOException {
		
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
		File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenShot,new File("C:/Users/juanj/eclipse-workspace/Liverpool_ScreenShoots/LVP_" 
							+ timeStamp +".png"));

	}
	
	
	
	
	
	public void highLightElements(WebDriver driver, WebElement element) { 
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid blue;');", element);
		implicitWait(4);
		
		
	}
	
	

	
	
}
