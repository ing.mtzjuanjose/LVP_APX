package com.liverpool.pageobjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.liverpool.reusablemethods.ReusableMethods;



public class LandingPage extends ReusableMethods {
	
	WebDriver driver;

	public LandingPage(WebDriver driver) {
		
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@id='mainSearchbar']")
	WebElement searchTextBox;
	
	@FindBy(css="div[class='input-group-append'] i")
	WebElement searchButton;
	
	@FindBy(className ="a-plp-color")
	WebElement colorOption;


	
	
	public void goTo() {
			
			driver.get("https://www.liverpool.com.mx/tienda/home");
		}

	public void searchProductsBar(String productName) throws IOException {
		
		takeScreenShot();
		highLightElements(driver, searchTextBox);
		searchTextBox.sendKeys(productName);
		takeScreenShot();
		searchButton.click();
		
		}
}
