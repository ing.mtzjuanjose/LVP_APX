package com.liverpool.pageobjects;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.WheelInput.ScrollOrigin;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.liverpool.reusablemethods.ReusableMethods;

public class ProductsList extends ReusableMethods {
	
	WebDriver driver;
	
	
	public ProductsList(WebDriver driver) {
		
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(className ="a-plp-color")
	WebElement colorOption;
	
	@FindBy(id ="search-filter-brands")
	WebElement searchFilterBrands;
	

	@FindBy(xpath ="//input[@id='brand-JBL']")
	WebElement checkboxJbl;
	
	@FindBy(xpath ="//div[@class='o-listing__products']/ul/li[4]")
	//div/ul/li[@class='m-product__card card-masonry a'][4] 
	WebElement fourthSpeaker;
	
	
	@Test
	public void selectCharProduct(String colorSelection, String brandName) throws InterruptedException, IOException {
		
		
		scrollToElement(colorOption, 6);
		highLightElements(driver, colorOption);
		colorSelection(colorSelection);
		
		scrollToElement(searchFilterBrands, 6);
		searchFilterBrands.sendKeys(brandName);
		
		checkboxJbl.click();
		scrollToElement(searchFilterBrands, 6);
		implicitWait(10);
		checkboxJbl.click();
		searchFilterBrands.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
	
		implicitWait(10);
		
		scrollToElement(fourthSpeaker, 10);
		fourthSpeaker.click();
		
	}
	
}
