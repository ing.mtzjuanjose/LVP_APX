package com.liverpool.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.liverpool.reusablemethods.ReusableMethods;

public class BoseSpeaker extends ReusableMethods {
	
	WebDriver driver;
	
	
	public BoseSpeaker(WebDriver driver) {
		
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(id ="opc_pdp_addCartButton")
	WebElement addToBag;
	
	@FindBy(id ="opc_pdp_buyNowButton")
	WebElement buyNow;
	
	@FindBy(xpath ="//button[@class='a-header__bag']")
	WebElement shoppingBagButton;
	
	@FindBy(linkText ="Comprar")
	WebElement buyButton;
	
	
	 @Override
	public void implicitWait(long waitTimeInSeconds) {
		// TODO Auto-generated method stub
		super.implicitWait(10);
	}
	public void addToShoppingBag() {
	
		addToBag.click();
		shoppingBagButton.click();
		buyButton.click();
		
		
	}
	
//	public void buyProductNow() {
//		buyNow.click();
//	
//	}
	
	
	
	
	

}
