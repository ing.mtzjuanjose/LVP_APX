package com.liverpool;


import java.io.IOException;
import java.security.ProtectionDomain;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.liverpool.pageobjects.BoseSpeaker;
import com.liverpool.pageobjects.LandingPage;
import com.liverpool.pageobjects.ProductsList;

public class SearchProductsTest {
	
	
		private WebDriver driver;
		@BeforeClass
		public  void setUp() {
			
		System.setProperty("webdriver.chrome.driver", "C:/Users/chromedriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		
		}
		
		
		@Test(groups = {"Scenario1"})
		public void addProductToShoppingBagBlack() throws InterruptedException, IOException {
			
			LandingPage landingPage = new LandingPage(driver);
			landingPage.goTo();
			landingPage.searchProductsBar("Bocina bluetooth");
			
			ProductsList productsList = new ProductsList(driver);
			productsList.selectCharProduct("Negro", "JBL");
			
			BoseSpeaker boseSpeaker = new BoseSpeaker(driver);
			boseSpeaker.addToShoppingBag();
			
		}
		
		
		
		@Test(groups = {"Scenario2"})
		public void addProductToShoppingBagRed() throws InterruptedException, IOException {
			
			LandingPage landingPage = new LandingPage(driver);
			landingPage.goTo();
			landingPage.searchProductsBar("Bocina bluetooth");
			
			ProductsList productsList = new ProductsList(driver);
			productsList.selectCharProduct("Rojo", "JBL");
			
			BoseSpeaker boseSpeaker = new BoseSpeaker(driver);
			boseSpeaker.addToShoppingBag();
			
		}
		
		
		
		
	
		
		
		
		@AfterClass
		public  void closeInstances() {
		
			//driver.close();
			
		}	
		
	 }	

